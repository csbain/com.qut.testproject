package com.qut.testproject.controller;


import com.qut.testproject.domain.Choice;
import com.qut.testproject.domain.Question;
import com.qut.testproject.dto.QuestionRequest;
import com.qut.testproject.repository.ChoiceRepository;
import com.qut.testproject.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/")
public class QuestionController {

    @Autowired
    private ChoiceRepository choiceRepository;

    @Autowired
    private QuestionRepository questionRepository;


    @RequestMapping(
            method = RequestMethod.GET,
            value = "/"
    )
    ResponseEntity<Map<String,String>> getBase(){
        Map<String,String> response = new HashMap<String,String>(){{
            put("questions_url","/questions");
        }};
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }


    private Integer questionsPerPage = 20;




    @RequestMapping(
            method = RequestMethod.GET,
            value = "/questions"
    )
    ResponseEntity<List<Question>> getQuestionsPage(@RequestParam(value="page", defaultValue="1") Integer page){
        Integer pageStart = (questionsPerPage*page)-(questionsPerPage);
        Integer pageEnd = (questionsPerPage*page)-1;
        List<Question> response = questionRepository.findAll(new PageRequest(pageStart, pageEnd)).getContent();
        Long questionTotal = questionRepository.count();
        if(Long.valueOf(pageEnd) < questionTotal){
            String nextUrl = "/questions?page="+(page+1);
            return ResponseEntity.status(HttpStatus.OK)
                    .header("Link","<"+nextUrl+">; rel=\"next\"").body(response);
        }else{
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }
    }


    @RequestMapping(
            method = RequestMethod.POST,
            value = "/questions"
    )
    @Transactional
    public ResponseEntity<Question> postChars(
            @RequestBody QuestionRequest questionRequest
    ) {
        Question question = new Question(questionRequest.getQuestion());
        question = questionRepository.save(question);
        List<String> choices = questionRequest.getChoices();

        for (Integer i=0; i < choices.size(); i++){
            question.addChoice(new Choice(i+1, choices.get(i)));
        }
        question = questionRepository.save(question);

        return ResponseEntity.status(HttpStatus.CREATED).header("Location",question.getUrl()).body(question);
    }


    @RequestMapping(
            method = RequestMethod.GET,
            value = "/questions/{question_id}"
    )
    ResponseEntity<Question> getQuestion(@PathVariable(value = "question_id") Integer question_id,
                                         HttpServletRequest request){
        Question question = questionRepository.findOne(question_id);
        return ResponseEntity.status(HttpStatus.OK).body(question);
    }


    @RequestMapping(
            method = RequestMethod.GET,
            value = "/questions/{question_id}/choices/{choice_id}"
    )
    ResponseEntity<Choice> getQuestionChoice(@PathVariable(value = "question_id") Integer question_id,
                                               @PathVariable(value = "choice_id") Integer choice_id){
        Question question = questionRepository.findOne(question_id);
        Choice foundChoice = null;
        for (Choice choice: question.getChoices()) {
            if(choice.getChoice_id() == choice_id){
                foundChoice = choice;
                break;
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(foundChoice);
    }


    @RequestMapping(
            method = RequestMethod.POST,
            value = "/questions/{question_id}/choices/{choice_id}"
    )
    ResponseEntity<Choice> voteQuestionChoice(@PathVariable(value = "question_id") Integer question_id,
                                             @PathVariable(value = "choice_id") Integer choice_id){
        Question question = questionRepository.findOne(question_id);
        Choice foundChoice = null;
        for (Choice choice: question.getChoices()) {
            if(choice.getChoice_id() == choice_id){
                foundChoice = choice;
                break;
            }
        }
        foundChoice.voteUp();
        choiceRepository.save(foundChoice);
        return ResponseEntity.status(HttpStatus.CREATED).header("Location",question.getUrl()).body(foundChoice);
    }

}
