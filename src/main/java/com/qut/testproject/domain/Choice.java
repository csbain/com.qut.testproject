package com.qut.testproject.domain;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Choice implements Serializable {


    public Choice(){

    }

    public Choice(Integer choice_id, String choice){
        this.choice_id=choice_id;
        this.choice=choice;

    }

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id", insertable=true, updatable=true, unique=true, nullable=false)
    @JsonIgnore
    private Integer id;

    @Column
    @JsonIgnore
    private Integer choice_id;


    @Column
    private String choice;


    @Column
    private Integer votes =0;

    @Transient
    private String url;

    @JsonIgnore
    private Question question;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getUrl() {
        return url;
    }
    public void setUrl(String url){
        this.url = url;
    }

    public Integer getChoice_id() {
        return choice_id;
    }

    public void setChoice_id(Integer choice_id) {
        this.choice_id = choice_id;
    }


    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }



    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    public void voteUp(){
        this.votes+=1;
    }

    public void voteDown(){
        this.votes-=1;
    }



    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

}
