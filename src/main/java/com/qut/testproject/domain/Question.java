package com.qut.testproject.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Question implements Serializable {


    public Question(){
    }

    public Question(String question){
        this.question = question;
        this.choices = new ArrayList<>();
        this.published_at = new Date();

    }




    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id", insertable=true, updatable=true, unique=true, nullable=false)
    @JsonIgnore
    private Integer id;


    @Column
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssZ")
    private Date published_at;


    @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    private List<Choice> choices;

    @Column
    public String question;



    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer question_id) {
        this.id = id;
    }

    public Date getPublished_at() {
        return published_at;
    }

    public void setPublished_at(Date published_at) {
        this.published_at = published_at;
    }

    @JsonProperty(value = "url")
    public String getUrl() {
        return  "/questions/"+id.toString();
    }



    public List<Choice> getChoices() {
        for (Choice choice: this.choices) {
            choice.setUrl("/questions/"+this.getId()+"/choices/"+choice.getId());
        }

        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public void addChoice(Choice choice){
        this.choices.add(choice);
    }
}
