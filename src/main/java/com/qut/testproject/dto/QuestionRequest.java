package com.qut.testproject.dto;

import java.util.ArrayList;
import java.util.List;

public class QuestionRequest {

    private String question;
    private List<String> choices = new ArrayList<>();


    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getChoices() {
        return choices;
    }

    public void setChoices(List<String> choices) {
        this.choices = choices;
    }



}
