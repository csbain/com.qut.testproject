package com.qut.testproject.repository;

import com.qut.testproject.domain.Choice;
import com.qut.testproject.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ChoiceRepository extends JpaRepository<Choice, Integer>{

    @Query("select c from Choice c where c.question = ?1 AND c.choice_id = ?2")
    Choice findChoiceFromQuestion(Question question, Integer choice_id);

}
